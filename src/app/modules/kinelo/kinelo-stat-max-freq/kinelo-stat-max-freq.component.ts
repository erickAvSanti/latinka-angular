import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { IKineloData } from 'src/app/interfaces/ikinelo-data';

@Component({
  selector: 'app-kinelo-stat-max-freq',
  templateUrl: './kinelo-stat-max-freq.component.html',
  styleUrls: ['./kinelo-stat-max-freq.component.scss']
})
export class KineloStatMaxFreqComponent implements OnInit, OnChanges {

  @Input() data: Array<IKineloData> = [];
  constructor() { }

  ngOnInit(): void {
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
    if(changes['data']?.currentValue){
      this.applyStatistic(changes['data'].currentValue);
    }
  }

  max_frequencies: any = {};
  max_frequencies_arr: Array<any> = [];
  applyStatistic(data: Array<IKineloData>): void{
    this.max_frequencies = {};
    this.max_frequencies_arr = [];
    [...new Array(80).keys()].map(value => value + 1).forEach(value => {
      this.max_frequencies[`${value}`] = 0;
    });
    data.forEach( prop => {
      if(prop.valores){
        prop.valores.forEach(valor => {
          this.max_frequencies[`${parseInt(`${valor}`, 10)}`]++;
        });
      }
    });
    console.log(this.max_frequencies);
    this.max_frequencies_arr = Object.entries(this.max_frequencies).sort((a: Array<any>,b: Array<any>) => b[1]-a[1]);
  }
}
