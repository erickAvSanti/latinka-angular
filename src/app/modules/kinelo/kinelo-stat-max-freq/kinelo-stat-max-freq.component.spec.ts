import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KineloStatMaxFreqComponent } from './kinelo-stat-max-freq.component';

describe('KineloStatMaxFreqComponent', () => {
  let component: KineloStatMaxFreqComponent;
  let fixture: ComponentFixture<KineloStatMaxFreqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KineloStatMaxFreqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KineloStatMaxFreqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
