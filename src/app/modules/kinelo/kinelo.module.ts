import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KineloRoutingModule } from './kinelo-routing.module';
import { MaterialModule } from '../material/material.module';
import { KineloDashboardComponent } from './kinelo-dashboard/kinelo-dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KineloStatisticsComponent } from './kinelo-statistics/kinelo-statistics.component';
import { KineloStatMaxFreqComponent } from './kinelo-stat-max-freq/kinelo-stat-max-freq.component';


@NgModule({
  declarations: [
    KineloDashboardComponent,
    KineloStatisticsComponent,
    KineloStatMaxFreqComponent
  ],
  imports: [
    CommonModule,
    KineloRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
  ]
})
export class KineloModule { }
