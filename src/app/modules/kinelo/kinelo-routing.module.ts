import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KineloDashboardComponent } from './kinelo-dashboard/kinelo-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: KineloDashboardComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KineloRoutingModule { }
