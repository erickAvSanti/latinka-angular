import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KineloDashboardComponent } from './kinelo-dashboard.component';

describe('KineloDashboardComponent', () => {
  let component: KineloDashboardComponent;
  let fixture: ComponentFixture<KineloDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KineloDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KineloDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
