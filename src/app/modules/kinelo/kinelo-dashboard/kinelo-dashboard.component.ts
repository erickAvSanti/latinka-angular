import { AfterViewInit, Component, isDevMode, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import { filter, noop, timer } from 'rxjs';
import { IKineloData } from 'src/app/interfaces/ikinelo-data';
import { KineloService } from 'src/app/services/kinelo.service';

@Component({
  selector: 'app-kinelo-dashboard',
  templateUrl: './kinelo-dashboard.component.html',
  styleUrls: ['./kinelo-dashboard.component.scss'],
  providers: [
    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
})
export class KineloDashboardComponent implements OnInit, AfterViewInit, OnChanges {

  displayedColumns: string[] = [
    'index', 
    'sorteo', 
    'clipboard', 
    'hora', 
    'valores',
    'acciones',
  ];
  dataSource!: MatTableDataSource<Array<IKineloData>>;
  data: Array<IKineloData> = [];

  currentDate = new FormControl(moment().subtract(1, 'day'));
  lasResultsText = new FormControl('');

  compareAction: {row: IKineloData | null, max: number} = {row: null, max: 0};

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private kineloService: KineloService,
  ) {
  }
  ngOnInit(): void {
    return;
  }
  reload(): void{
    this.getDataFor();
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  getDataFor(): void{
    this.kineloService.getData(this.currentDate.value.format("DD/MM/YYYY"), this.currentDate.value.isSame(moment(), 'day'))
    .subscribe({
      next: onSucces => {
        if(isDevMode()){
          console.log(onSucces);
        }
        this.data = onSucces;
        this.dataSource = new MatTableDataSource(onSucces);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      },
      error: onError => {

      },
      complete: noop,
    });
  }

  ngAfterViewInit() {
    this.getDataFor();
  }
  selectedNumbers: Array<string> = [];
  maxFrequencies: Array<any> = [];
  maxFrequenciesSet: {[key: string]: number} = {};
  toggleSelectionNumber(value: string): void{
    if(this.selectedNumbers.includes(value)){
      this.selectedNumbers = this.selectedNumbers.filter(v => v !== value);
    }else{
      this.selectedNumbers.push(value);
    }
  }
  numberIsSelected(value: string): boolean{
    return this.selectedNumbers.includes(value);
  }
  selectOnlyThese(row: IKineloData): void{
    const tmp = this.selectedNumbers.filter(n => row.valores.includes(n));
    if(tmp.length === row.valores.length){
      this.selectedNumbers = this.selectedNumbers.filter(n => !row.valores.includes(n));
    }else{
      this.selectedNumbers = row.valores.slice();
    }
    console.log(this.selectedNumbers);
  }
  compareLastFor(max: number, row: IKineloData, idx: number): void {
    this.compareAction.row = row;
    this.compareAction.max = max;
    this.maxFrequencies = [];
    const exactIndex = idx + this.paginator.pageIndex * this.paginator.pageSize;
    const mapping: {[key: number]: number} = {};
    [...new Array(80).keys()].map(value => value + 1).forEach( value => mapping[value] = 0);
    [...new Array(max).keys()].forEach(index => {
      if( exactIndex + index < this.data.length){
        this.data[exactIndex + index].valores.forEach( (value: any) => {
          mapping[parseInt(value, 10)]++;
        });
      }
    });
    const sorted = Object.entries(mapping).sort((a: any, b: any) => b[1] - a[1]);
    console.log(JSON.stringify(sorted));
    let filtered = sorted.filter(props => props[1] > 1);
    if(!filtered.length){
      filtered = sorted.filter(props => props[1] > 0);
    }
    this.maxFrequencies = filtered;
    const tmp = filtered.map(props => props[0]);
    console.log(tmp.sort( (a,b) => parseInt(a) - parseInt(b) ));
    this.selectedNumbers = tmp;
    this.maxFrequenciesSet = {};
    filtered.forEach(prop => {
      this.maxFrequenciesSet[prop[0]] = prop[1];
    });

    console.log(`Para 4 jugadas:`);
    [...new Array(4).keys()].forEach(index => {
      this.playForNumbers(4, tmp);
    });

  }
  playForNumbers(maxSelection: number, hightSelectedNumbers: Array<any>): void{
    
    let tmp = [...hightSelectedNumbers];
    const selectedValues: Array<string> = [];
    [...new Array(maxSelection).keys()].forEach(index => {
      const selectedIndex = Math.round(this.randomRange(0, tmp.length - 1));
      const selectedValue = tmp[selectedIndex];
      tmp.splice(selectedIndex, 1);
      selectedValues.push(selectedValue);
    });
    let splittedText: Array<number> = [];
    if(this.lasResultsText.value.length){
      splittedText = this.lasResultsText.value.split(/ +/).map( (str: string) => parseInt(str, 10));
    }
    const hits = selectedValues.filter(value => splittedText.includes(parseInt(value, 10)));
    const additional = splittedText.length ?  `, aciertos = ${hits.length}, ${JSON.stringify(hits)}${hits.length == maxSelection ? ' <-----' : ''}` : '';
    console.log(`Debes elegir: ${JSON.stringify(selectedValues)} ${additional}`);
  }
  randomRange(min: number, max: number): number{
    return Math.random() * (max - min) + min;
  }
  clipBoardData(item: IKineloData): string{
    return item.valores.join(' ');
  }
  selectedValuesToCopied: IKineloData | null = null;
  selectValuesToText(item: IKineloData): void{
    if(this.selectedValuesToCopied == item){
      this.selectedValuesToCopied = null;
      this.lasResultsText.setValue('');
    }else{
      this.selectedValuesToCopied = item;
      this.lasResultsText.setValue(item.valores.join(' '));
    }
  }
}
