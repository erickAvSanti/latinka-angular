import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KineloStatisticsComponent } from './kinelo-statistics.component';

describe('KineloStatisticsComponent', () => {
  let component: KineloStatisticsComponent;
  let fixture: ComponentFixture<KineloStatisticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KineloStatisticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KineloStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
