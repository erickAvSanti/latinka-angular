import { Component, Input, OnInit } from '@angular/core';
import { IKineloData } from 'src/app/interfaces/ikinelo-data';

@Component({
  selector: 'app-kinelo-statistics',
  templateUrl: './kinelo-statistics.component.html',
  styleUrls: ['./kinelo-statistics.component.scss']
})
export class KineloStatisticsComponent implements OnInit {

  @Input() data: Array<IKineloData> = [];
  items = ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5'];
  expandedIndex = 0;
  constructor() { }

  ngOnInit(): void {
  }

}
