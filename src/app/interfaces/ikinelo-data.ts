export interface IKineloData {
  sorteo: string;
  fecha: string;
  valores: Array<string>;
}
