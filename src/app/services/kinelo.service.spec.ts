import { TestBed } from '@angular/core/testing';

import { KineloService } from './kinelo.service';

describe('KineloService', () => {
  let service: KineloService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KineloService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
