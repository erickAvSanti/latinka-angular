import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class KineloService {

  constructor(
    private http: HttpClient,
  ) { }

  getData(date: string, reload = false): Observable<any>{
    const params = new HttpParams().set('date', date).set('reload', reload ? 1 : 0);
    return this.http.get(`${environment.api}/kinelo/list`, {params});
  }
}
